#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT            8080
#define BUFFER_LEN      4096

int main() {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in serv_addr;
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

    connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    
    char buffer[BUFFER_LEN] = {0};
    recv(sockfd, buffer, sizeof(buffer), 0);
    std::cout << "Message from server: " << buffer << std::endl;

    close(sockfd);
    return 0;
}