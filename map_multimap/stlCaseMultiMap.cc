#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <ctime>

#define SALE        1
#define RD          2
#define FINACIAL    3
#define ALL         4

const char* kDepartment[] = {"Sale", "RD", "Finacial", "All"};

class Employee {
public:
    Employee() : _age(0), _money(0.0f) {}
    Employee(const std::string& name, const std::string& tel, unsigned age, float money) : 
             _name(name), _tel(tel), _age(age), _money(money) {}

    const std::string&  name() const { return _name; }
    const std::string&  tel() const { return _tel; }
    unsigned            age() const { return _age; }
    float               money() const { return _money; }

private:
    std::string _name;
    std::string _tel;
    unsigned    _age;
    float       _money;
};

void createEmployees(std::vector<Employee>& employees)
{
    // 设置种子 (通常使用时间)
    std::srand(std::time(0));
    std::string en = "ABCDE";
    std::string prefix = "Employee ";
    for (unsigned i = 0; i < en.size(); ++i) {
        std::string name = prefix + en[i];
        std::string tel = std::to_string(rand());
        unsigned age = rand() % 100;
        float money = 20000 + rand() % 10 * 1000;
        employees.emplace_back(Employee(name, tel, age, money));
    }
}

void joinDepartment(const std::vector<Employee>& employees, 
                    std::multimap<int, Employee>& infos)
{
    // 设置种子 (通常使用时间)
    std::srand(std::time(0));
    for (auto& ep : employees) {
        int department = rand() % 3;
        while (infos.count(department + 1) > 1)
            department = rand() % 3;
        std::cout << ep.name() << " join department " << department << " " 
                  << kDepartment[department] << std::endl;
        infos.emplace(department + 1, ep);
    }
}

void showEmployees(const  std::multimap<int, Employee>& infos)
{
    std::cout << "Please select the department you want to view:\n"
              << "1) Sale\t2) RD\t3) Finacial\n";
    int num;
    std::cin >> num;

    switch (num) {
    case 1:
        std::cout << "Employees of Department Sale:" << std::endl;
        break;
    case 2:
        std::cout << "Employees of Department RD:" << std::endl;
        break;
    case 3:
        std::cout << "Employees of Department Finacial:" << std::endl;
        break;
    default:
        std::cout << "Unknow Department ID: " << num << std::endl;
        return;
    }
    auto it = infos.find(num);
    if (it == infos.end())
        return;
    int count = infos.count(num);
    for (int i = 0; i < count; ++i, ++it) {
        std::cout << it->second.name() << ", "
                  << it->second.age() << ", "
                  << it->second.money() << ", "
                  << it->second.tel() << std::endl;
    }
}

void test()
{
    // 创建员工
    std::vector<Employee> employees;
    createEmployees(employees);

    // add to department
    std::multimap<int, Employee> infos;
    joinDepartment(employees, infos);

    showEmployees(infos);
}

int main(int argc, char** argv)
{
    test();
    return 0;
}
