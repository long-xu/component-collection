#include <iostream>
#include <string>
#include <map>

class Person {
public:
    Person() : _id(0), _score(0.0f) {}
    Person(int id, const std::string& name, float score) :
            _id(id), _name(name), _score(score) {}
    
    int         id() const { return _id; }
    std::string name() const { return _name; }
    float       score() const { return _score; }

private:
    int         _id;
    std::string _name;
    float       _score;
};



void printAllMap(const std::map<int, Person>& m)
{
    for (const auto& it : m) {
        std::cout << it.first << " -> " << it.second.id() << ", " 
        << it.second.name() << ", " << it.second.score() << std::endl;
    }
}

void test()
{
    std::map<int, Person> m;
    // 方式一：
    m.insert(std::pair<int, Person>(101, Person(101, "fly", 98.8f)));
    // 方式二：
    m.insert(std::make_pair(102, Person(102, "Lion", 98.9f)));
    // 方式三：
    m.insert(std::map<int, Person>::value_type(103, Person(103, "FLY", 99.8f)));
    // 方式四：
    m.emplace(104, Person(104, "Long", 99.8f));
    // 方式五：
    m[105] = Person(105, "Lion Long", 99.8f);

    printAllMap(m);

    std::cout << m[106].id() << ", " << m[106].name() << ", " << m[106].score() << std::endl;

    std::cout << "---------- 分割线 ----------" << std::endl;

    printAllMap(m);
}

int main(int argc, char** argv)
{
    test();
    return 0;
}